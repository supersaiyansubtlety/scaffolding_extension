- 1.1.7 (29 Dec. 2023): Updated for 1.20.2-4
- 1.1.6 (23 Jun. 2023): Marked as compatible with 1.20 and 1.20.1
- 1.1.5 (4 Apr. 2023): Marked as compatible with 1.19.3 and 1.19.4
- 1.1.4 (7 Aug. 2022): Marked as compatible with 1.19.2
- 1.1.3 (29 Jul. 2022): 
  - Marked as compatible with 1.19.1
  - Minor internal changes
- 1.1.2 (3 Jul. 2022): Updated for 1.19!
- 1.1.1 (3 Jul. 2022): Updated for 1.18.2!
- 1.1 (3 Jan. 2022):
  
  First public release!
  
  Rewritten to use statement lib (thanks [Virtuoel](https://github.com/Virtuoel/)!). 
  
  Statement lib is required and must be downloaded separately: 
  - from [Modrinth](https://modrinth.com/mod/statement)
  - or from [CurseForge](https://www.curseforge.com/minecraft/mc-mods/statement)

  The mod is now server-sided (works in singleplayer too)!

- 1.0 (12 Mar. 2021): Initial version. 