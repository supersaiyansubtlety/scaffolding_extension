package net.sssubtlety.scaffolding_extension.mixin;

import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ScaffoldingItem;
import net.sssubtlety.scaffolding_extension.ScaffoldingExtension;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;


@Mixin(value = ScaffoldingItem.class, priority = 1100)
public abstract class ScaffoldingItemMixin {
    @ModifyConstant(method = "getPlacementContext", constant = @Constant(intValue = 7))
    private static int scaffolding_extension$calculateDistanceModifyMax(int originalDist, ItemPlacementContext context) {
        return ScaffoldingExtension.getScaffoldingLimit(context.getWorld());
    }
}
