package net.sssubtlety.scaffolding_extension.mixin;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ScaffoldingBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.Property;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.random.RandomGenerator;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;
import net.sssubtlety.scaffolding_extension.ScaffoldingExtension;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.*;

import static net.sssubtlety.scaffolding_extension.ScaffoldingExtension.EXTENSION_DISTANCE;

@Mixin(value = ScaffoldingBlock.class, priority = 1100)
abstract class ScaffoldingBlockMixin extends Block {
    private ScaffoldingBlockMixin(Settings settings) {
        super(settings);
        throw new IllegalStateException("ScaffoldingBlockMixin's dummy constructor called!");
    }

    @ModifyConstant(method = "canPlaceAt", constant = @Constant(intValue = 7))
    private static int canPlaceAtModifyMax(int originalDist, BlockState state, WorldView world, BlockPos pos) {
        return ScaffoldingExtension.getScaffoldingLimit((World) world);
    }

    @ModifyConstant(method = "calculateDistance", constant = @Constant(intValue = 7))
    private static int calculateDistanceModifyMax(int originalDist, BlockView world, BlockPos pos) {
        return ScaffoldingExtension.getScaffoldingLimit((World) world);
    }

    @Redirect(method = "calculateDistance", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/BlockState;get(Lnet/minecraft/state/property/Property;)Ljava/lang/Comparable;"))
    private static Comparable<Integer> calculateDistanceGetDistance(BlockState stateInstance, Property<Integer> property) {
        return ScaffoldingExtension.getExtendedDistanceComparable(stateInstance);
    }

    @ModifyConstant(method = "scheduledTick", constant = @Constant(intValue = 7))
    private int allowExtension(int originalDist, BlockState state, ServerWorld world, BlockPos pos, RandomGenerator random) {
        return ScaffoldingExtension.getScaffoldingLimit(world);
    }

    @Redirect(method = "scheduledTick", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/BlockState;get(Lnet/minecraft/state/property/Property;)Ljava/lang/Comparable;"))
    private Comparable<Integer> scheduledTickGetDistance(BlockState stateInstance, Property<Integer> property) {
        return ScaffoldingExtension.getExtendedDistanceComparable(stateInstance);
    }

    @Redirect(
            method = "scheduledTick",
            at = @At(
                    value = "INVOKE",
                    ordinal = 0,
                    target = "Lnet/minecraft/block/BlockState;with(Lnet/minecraft/state/property/Property;Ljava/lang/Comparable;)Ljava/lang/Object;"
            ),
            slice = @Slice(from = @At(value = "FIELD", target = "Lnet/minecraft/block/ScaffoldingBlock;DISTANCE:Lnet/minecraft/state/property/IntProperty;"))
    )
    private Object scheduledTickWithMultipliedDist(BlockState stateInstance, Property<Integer> property, Comparable<Integer> comparable) {
        return stateWithMultipliedDist(stateInstance, (Integer) comparable);
    }

    // targets the first `BlockState::with` call after accessing `DISTANCE`
    @Redirect(
            method = "getPlacementState",
            at = @At(
                    value = "INVOKE",
                    ordinal = 0,
                    target = "Lnet/minecraft/block/BlockState;with(Lnet/minecraft/state/property/Property;Ljava/lang/Comparable;)Ljava/lang/Object;"
            ),
            slice = @Slice(from = @At(value = "FIELD", target = "Lnet/minecraft/block/ScaffoldingBlock;DISTANCE:Lnet/minecraft/state/property/IntProperty;"))
    )
    private Object placementStateWithMultipliedDist(BlockState stateInstance, Property<Integer> property, Comparable<Integer> comparable) {
        return stateWithMultipliedDist(stateInstance, (Integer) comparable);
    }

    @Unique
    private Object stateWithMultipliedDist(BlockState stateInstance, Integer comparable) {
        return stateInstance
                .with(ScaffoldingBlock.DISTANCE, comparable % 8)
                .with(EXTENSION_DISTANCE, comparable / 8);
    }
}
