package net.sssubtlety.scaffolding_extension;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.minecraft.block.Blocks;
import net.minecraft.world.GameRules;
import virtuoel.statement.api.StateRefresher;

public class Init implements ModInitializer {
    static final GameRules.Key<GameRules.IntGameRule> SCAFFOLDING_LIMIT = GameRuleRegistry.register(ScaffoldingExtension.NAMESPACE + ":limit", GameRules.Category.MISC, GameRuleFactory.createIntRule(32, 1, ScaffoldingExtension.MAX_LIMIT));

    static {
        ServerLifecycleEvents.SERVER_STARTED.register(server -> ScaffoldingExtension.world = server.getOverworld());
        StateRefresher.INSTANCE.addBlockProperty(Blocks.SCAFFOLDING, ScaffoldingExtension.EXTENSION_DISTANCE, 0);
        StateRefresher.INSTANCE.reorderBlockStates();
    }

    @Override
    public void onInitialize() { }
}
