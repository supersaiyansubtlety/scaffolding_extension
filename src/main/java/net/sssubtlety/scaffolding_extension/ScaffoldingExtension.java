package net.sssubtlety.scaffolding_extension;

import net.minecraft.block.BlockState;
import net.minecraft.block.ScaffoldingBlock;
import net.minecraft.state.property.IntProperty;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;

import static net.sssubtlety.scaffolding_extension.Init.SCAFFOLDING_LIMIT;

public class ScaffoldingExtension {
	public static final String NAMESPACE = "scaffolding_extension";
	public static final int MAX_LIMIT = 63;
	public static final IntProperty EXTENSION_DISTANCE = IntProperty.of("extension_distance", 0, 7);

	static World world;

	public static World getWorld() {
		return world;
	}

	public static int getScaffoldingLimit(World world) {
		return world.getGameRules().get(SCAFFOLDING_LIMIT).getValue();
	}

	@NotNull
	public static Comparable<Integer> getExtendedDistanceComparable(BlockState state) {
		return getExtendedDistance(state);
	}

	public static int getExtendedDistance(BlockState state) {
		return state.get(ScaffoldingBlock.DISTANCE) + state.get(EXTENSION_DISTANCE) * 8;
	}
}
