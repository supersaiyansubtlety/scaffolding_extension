package net.sssubtlety.scaffolding_extension;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ScaffoldingBlock;
import net.minecraft.util.collection.IdList;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import virtuoel.statement.api.StatementApi;

import java.util.OptionalInt;
import java.util.function.BiFunction;
import java.util.function.Function;

import static net.minecraft.block.Block.getRawIdFromState;
import static net.sssubtlety.scaffolding_extension.ScaffoldingExtension.*;

public class ScaffoldingStateSync implements StatementApi {
    @Override
    public <S, L extends Iterable<S>> OptionalInt getSyncedId(L idList, @Nullable S state, BiFunction<L, S, Integer> idFunc, BiFunction<L, Integer, S> getFunc, Function<L, Integer> sizeFunc) {
        if (state instanceof BlockState blockState && blockState.isOf(Blocks.SCAFFOLDING)) {
            final int distance = getExtendedDistance(blockState);
            // less than vanilla limit

            final World world = ScaffoldingExtension.getWorld();
            if (world != null) {
                final int limit = getScaffoldingLimit(world);
                final int difference = limit - distance;

                if (difference < ScaffoldingBlock.MAX_DISTANCE) {
                    // sync the last 6 states as the last 6 vanilla states, for resourcepacks
                    return OptionalInt.of(getRawIdFromState(blockState
                            .with(ScaffoldingBlock.DISTANCE, ScaffoldingBlock.MAX_DISTANCE - difference)
                            .with(EXTENSION_DISTANCE, 0)
                    ));
                }
            }
            // otherwise sync 0 distance
            return OptionalInt.of(getRawIdFromState(blockState
                    .with(ScaffoldingBlock.DISTANCE, 0)
                    .with(EXTENSION_DISTANCE, 0)
            ));
        }

        return StatementApi.super.getSyncedId(idList, state, idFunc, getFunc, sizeFunc);
    }

    @Override
    public  <S> boolean shouldDeferState(IdList<S> idList, S state) {
        if (state instanceof BlockState blockState && blockState.isOf(Blocks.SCAFFOLDING))
            return blockState.get(EXTENSION_DISTANCE) > 0;
        return false;
    }
}
